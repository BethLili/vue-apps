const routes = [
  {
    path: '/Calculator',
    name: 'Calculator',
    component: require('./pages/Calculator').default
  },
  {
    path: '/math-quiz',
    name: 'MathQuiz',
    component: require('./pages/math-quiz/MathQuiz').default
  },
  {
    path: '/to-do',
    name: 'ToDo',
    component: require('./pages/to-do/App').default
  },
]

export default routes;